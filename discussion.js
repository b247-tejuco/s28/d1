// [ SECTION ] CRUD Operations

	// CRUD Operations are the heart of any backend application
	// Mastering the CRUD Operation is essential for any developer.

// [ SECTION ] Inserting Documents (Create)

	// [ SUB-SECTION ] Insert One Document

		// Since MongoDB deals with objes as it's structure for documents, we can easily create them by providing objects into our methods

		// Syntax:
			// db.collectionName.insertOne({ object });

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "987653421",
		email: "janedoe@mail.com"
	},
	courses: [ "CSS", "Javascript", "Phyton" ],
	department: "none"
}); 

	// [ SUB-SECTION ] Insert Many Document

		// Syntax:
			// db.collectionName.insertMany( [{objectA}, {objectB}] );

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "98756412",
			email: "theoryofeverithing@mail.com"
		},
		courses: [ "Phyton", "React", "PHP" ],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "985464757",
			email: "neiltothemoon@mail.com"
		},
		courses: [ "React", "Laravel", "Sass" ],
		department: "none"
	}
]);

// [ SECTION ] Finding Documents (READ)

	// If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned

	// Syntax:
		// db.collectionName.find();
		// db.collectionName.find({ field: value });

db.users.find();

db.users.find({ firstName: "Stephen" });

// [ SECTION ] Updating Documents (Update)

	// [ SUB-SECTION ] Updating a Single Document

	// Create a document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000000",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
});	

	// To update firstName: "Test"
	// Syntax:
		// db.collectionName.updateOne( {criteria}, { $set: { field: value}});

db.users.updateOne(
	{ firstName: "Test"},
	{
		$set : {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "1234567890",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// [SUB-SECTION] Update Many

	// Syntax:
		// db.collectionName.updateMany( {criteria}, { $set: { field: value}});

db.users.updateMany(
	{ department: "none" },
	{
		$set: { department: "HR" }
	}
);


// [ SECTION ] Deleting Documents (DELETE)

	// Creating a document to delete

// syntax delete one
// db.collectionName.deleteOne({criteria});

db.users.deleteOne({
	firstName: "test"
});


//subsection delete many
// syntax
//  db.collectionName.deleteMany({criteria});

db.users.deleteMany ({
	department:"none"
})


//section advanced queries
	//retriving data with complex data structures is also a good skill
// for any developer to have

// query an embedded document
// db.users.find({
// 	contact: {
// 		
// 		email: "theoryofeverything@mail.com"
// 	}
// })  cant find because phone number is missing

db.users.find({
    contact: {
        phone: "98765441",
        email: "theoryofeverything@mail.com"
    }
})